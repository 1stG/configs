# @1stG/configs

[![GitHub Actions](https://github.com/1stg/configs/workflows/CI/badge.svg)](https://github.com/1stG/configs/actions/workflows/ci.yml)
[![GitHub release](https://img.shields.io/github/release/1stg/configs)](https://github.com/1stg/configs/releases)
[![Codacy Grade](https://img.shields.io/codacy/grade/ee1a96f680514c038128710a67f7e973)](https://app.codacy.com/gh/1stG/configs)
[![type-coverage](https://img.shields.io/badge/dynamic/json.svg?label=type-coverage&prefix=%E2%89%A5&suffix=%&query=$.typeCoverage.atLeast&uri=https%3A%2F%2Fraw.githubusercontent.com%2F1stG%2Fconfigs%2Fmaster%2Fpackage.json)](https://github.com/plantain-00/type-coverage)
[![David Dev](https://img.shields.io/david/dev/1stg/configs.svg)](https://david-dm.org/1stg/configs?type=dev)

[![Conventional Commits](https://img.shields.io/badge/conventional%20commits-1.0.0-yellow.svg)](https://conventionalcommits.org)
[![Renovate enabled](https://img.shields.io/badge/renovate-enabled-brightgreen.svg)](https://renovatebot.com/)
[![js-standard-style](https://img.shields.io/badge/code%20style-standard-brightgreen.svg)](http://standardjs.com)
[![Code Style: Prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)
[![changesets](https://img.shields.io/badge/maintained%20with-changesets-176de3.svg)](https://github.com/atlassian/changesets)

> Personal but Shareable Configurations for all 1stG.me projects.

## Changelog

Detailed changes for each release are documented in [CHANGELOG.md](./CHANGELOG.md).

## License

[MIT][] © [JounQin][]@[1stG.me][]

[1stg.me]: https://www.1stg.me
[jounqin]: https://GitHub.com/JounQin
[mit]: http://opensource.org/licenses/MIT
