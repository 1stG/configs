# Change Log

## [0.1.2](https://github.com/1stG/configs/compare/@1stg/simple-git-hooks@0.1.1...@1stg/simple-git-hooks@0.1.2) (2021-03-23)

### Bug Fixes

- @commitlint/cli should be installed automatically ([d69b4fb](https://github.com/1stG/configs/commit/d69b4fb2b42e2bfed76e4509c57b82a52f12d68c))

## 0.1.1 (2021-03-23)

**Note:** Version bump only for package @1stg/simple-git-hooks
