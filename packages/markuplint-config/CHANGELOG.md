# @1stg/markuplint-config

## 2.1.0

### Minor Changes

- [`af649e7`](https://github.com/1stG/configs/commit/af649e731f49166389a6ec4b177a5497c4477028) Thanks [@JounQin](https://github.com/JounQin)! - feat: bump deps, update node engine setting

## 2.0.0

### Major Changes

- [#86](https://github.com/1stG/configs/pull/86) [`e5a780c`](https://github.com/1stG/configs/commit/e5a780c39c7fc49a3009dbac4bc6bfa18beb3365) Thanks [@JounQin](https://github.com/JounQin)! - feat!: bump deps, drop node 12

## 1.0.2

### Patch Changes

- [#73](https://github.com/1stG/configs/pull/73) [`fe9be5d`](https://github.com/1stG/configs/commit/fe9be5db15d6db0dec7c8f6cd25f54e29fb10e5f) Thanks [@JounQin](https://github.com/JounQin)! - fix(deps): bump all (dev)Dependencies

## 1.0.1

### Patch Changes

- [#69](https://github.com/1stG/configs/pull/69) [`65d9e3f`](https://github.com/1stG/configs/commit/65d9e3fb654f3a8704405a1061ddc10e21a5f420) Thanks [@JounQin](https://github.com/JounQin)! - fix(stylelint): support JavaScript/TypeScript/Markdown files

## 1.0.0

### Major Changes

- [#67](https://github.com/1stG/configs/pull/67) [`a2ab1a9`](https://github.com/1stG/configs/commit/a2ab1a9b6a275d5c89c329f28780ada21f120b25) Thanks [@JounQin](https://github.com/JounQin)! - chore: bump deps, upgrade markuplint usage

## 0.2.0

### Minor Changes

- [#64](https://github.com/1stG/configs/pull/64) [`c9743ef`](https://github.com/1stG/configs/commit/c9743efe61c8724bbdcfa33394776f27a6a851d4) Thanks [@JounQin](https://github.com/JounQin)! - feat!: bump all upgradable dependencies, update related usages

## 0.1.1

### Patch Changes

- [#51](https://github.com/1stG/configs/pull/51) [`626afaa`](https://github.com/1stG/configs/commit/626afaad80e57fa7779a7a0ff332cd4dc4836062) Thanks [@JounQin](https://github.com/JounQin)! - feat: upgrade (dev)Dependencies

## 0.1.0

### Minor Changes

- [#44](https://github.com/1stG/configs/pull/44) [`258b0eb`](https://github.com/1stG/configs/commit/258b0eb5290bb4a80e4b44dd0771790ed84b4928) Thanks [@JounQin](https://github.com/JounQin)! - feat: integration with markuplint
